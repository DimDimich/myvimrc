call plug#begin('~/.vim/plugged')

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" autocompletion
" Plug 'Valloric/YouCompleteMe'

" auto add sympol pairs: {} '' []
" Plug 'jiangmiao/auto-pairs'

" Support for Git
Plug 'tpope/vim-fugitive'

" Show changed lines
Plug 'airblade/vim-gitgutter'

" Search
Plug 'kien/ctrlp.vim'

" colorscheme
Plug 'kiddos/malokai.vim'

" Navigation. Usage: \ + s    (leaderkey - \)
Plug 'easymotion/vim-easymotion'

" emmet 'Ctrl' + 'y' + ','
Plug 'mattn/emmet-vim'

" typescript syntax
Plug 'leafgarland/typescript-vim'
"Plug 'herringtondarkholme/yats'

" typescript tslint
Plug 'palantir/tslint'

" highlight whitespace
Plug 'bronson/vim-trailing-whitespace'

" whitespace trimmer
Plug 'derekprior/vim-trimmer'

" multicursors
" Plug 'terryma/vim-multiple-cursors'

" typescript syntax
Plug 'leafgarland/typescript-vim'

" javascript syntax
Plug 'pangloss/vim-javascript'

" hex editor plugin
" Simply editing a file in binary mode, e.g.,
" vim -b some_file.jpg
" will open it in hex mode.
" Also, you can use :Hexmode to switch between hex editing and normal editing.
Plug 'shougo/vinarise.vim'

" C-c, C-c REPL code run
Plug 'jpalardy/vim-slime'
let g:slime_target = "tmux"

" VSCode language service
Plug 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install()}}

" commenter plugin
Plug 'scrooloose/nerdcommenter'

" VS code theme
Plug 'tomasiser/vim-code-dark'

Plug 'othree/html5.vim'

" xml
Plug 'othree/xml.vim'

" js lib syntax
" Plug 'othree/javascript-libraries-syntax.vim'
" let g:used_javascript_libs = 'vue, jquery'

" Snippets
Plug 'msanders/snipmate.vim'

" c++ highlight
Plug 'octol/vim-cpp-enhanced-highlight'
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_experimental_simple_template_highlight = 1


" Initialize plugin system
call plug#end()

colorscheme codedark

syntax on
set number
set expandtab
set tabstop=2
set shiftwidth=2

set hlsearch
set incsearch

" mappings

map <C-n> :NERDTreeToggle<CR>
map <Leader> <Plug>(easymotion-prefix)

" for command mode
nnoremap <S-Tab> <<
" for insert mode
inoremap <S-Tab> <C-d>

" map multicursor plugin
" let g:multi_cursor_next_key='<C-d>'
" let &makeprg='(cd ./build && make)'
set backspace=indent,eol,start

set list
set listchars=
set listchars+=tab:░\
set listchars+=trail:·
set listchars+=extends:»
set listchars+=precedes:«
set listchars+=nbsp:⣿


" Ignore path for Ctrl-p
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'


set cursorline
set cursorcolumn

let g:user_emmet_leader_key='<C-Z>'
